package com.agero.nccsdk;

/**
 * Exception encapsulating all of the various underlying exceptions thrown by the SDK.
 */

public class NccException extends Exception {

    private final NccExceptionErrorCode errorCode;
    private final String TAG;

    /**
     *
     * @param TAG - TAG
     * @param message - error message
     * @param errorCode - error code
     */
    public NccException(String TAG, String message, NccExceptionErrorCode errorCode) {
        super(message);
        this.TAG = TAG;
        this.errorCode = errorCode;
    }

    /**
     * Get the error code
     *
     * @return error code
     */
    @SuppressWarnings("unused")
    public NccExceptionErrorCode getErrorCode() {
        return this.errorCode;
    }

    /**
     * Get the tag
     *
     * @return tag
     */
    @SuppressWarnings("unused")
    public String getTAG() {
        return this.TAG;
    }
}
