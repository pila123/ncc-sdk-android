package com.agero.nccsdk.internal.auth;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AuthManager {

    Completable authenticate(String apiKey);

    Single<Boolean> hasAuthenticated();
}
