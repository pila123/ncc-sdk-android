package com.agero.nccsdktestapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.agero.nccsdk.BuildConfig;
import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccPermissionsManager;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSdkInterface;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.TrackingContext;
import com.agero.nccsdk.adt.AdtStateMachine;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.adt.event.AdtEventListener;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getSimpleName();

    Button bStartUbi;
    Button bStopUbi;

    Button bStartTracking;
    Button bStopTracking;

    Button bAddTrackingListener;
    Button bRemoveTrackingListener;

    TextView tvSdkVersion;

    NccSdkInterface nccSdk;

    NccSensorListener realTimeSensorListener;

    private boolean hasRequestedWhitelistingDuringForegroundSession = false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bStartUbi = (Button) findViewById(R.id.button3);
        bStartUbi.setOnClickListener(startListener);

        bStopUbi = (Button) findViewById(R.id.button4);
        bStopUbi.setOnClickListener(stopListener);

        bStartTracking = (Button) findViewById(R.id.button5);
        bStartTracking.setOnClickListener(startListener);

        bStopTracking = (Button) findViewById(R.id.button6);
        bStopTracking.setOnClickListener(stopListener);

        bAddTrackingListener = (Button) findViewById(R.id.button7);
        bAddTrackingListener.setOnClickListener(startListener);

        bRemoveTrackingListener = (Button) findViewById(R.id.button8);
        bRemoveTrackingListener.setOnClickListener(stopListener);

        tvSdkVersion = (TextView) findViewById(R.id.tv_sdk_version);

        doSdkStuff();

        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            boolean isIgnoringBatteryOptimizations = powerManager.isIgnoringBatteryOptimizations(getPackageName());
            Log.e(MainActivity.class.getSimpleName(), "isIgnoringBatteryOptimizations: " + isIgnoringBatteryOptimizations);
        }
    }

    private void doSdkStuff() {
        nccSdk = NccSdk.getNccSdk(getApplication());

        nccSdk.setUserId("userId");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            nccSdk.setIgnoringBatteryOptimizationsCallback(() -> {
                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
                boolean isIgnoringBatteryOptimizations = powerManager.isIgnoringBatteryOptimizations(getPackageName());
                Log.e(MainActivity.class.getSimpleName(), "isIgnoringBatteryOptimizations: " + isIgnoringBatteryOptimizations);
                if (!isIgnoringBatteryOptimizations && !hasRequestedWhitelistingDuringForegroundSession) {
                    startActivity(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:" + getPackageName())));
                    hasRequestedWhitelistingDuringForegroundSession = true;
                }
            });
        }

        TrackingContext trackingContext = new TrackingContext();
        trackingContext.putBoolean("b", false);
        trackingContext.putDouble("d", 8768.5);
        trackingContext.putFloat("f", 9857.6f);
        trackingContext.putLong("l", 1000000);
        trackingContext.putString("s", "test trackingContext");
        nccSdk.setTrackingContext(trackingContext);

        nccSdk.addDrivingListener(new AdtEventListener() {
            @Override
            public void onDrivingStart() {
                Log.d(TAG, "Driving started!");
            }

            @Override
            public void onDrivingStop() {
                Log.d(TAG, "Driving stopped!");
            }
        });

        tvSdkVersion.setText("App version: " + getAppVersion(this) + ", SDK version: " + NccSdk.getSdkVersion() + " [" + BuildConfig.FLAVOR + "-" + BuildConfig.BUILD_TYPE + "]");
        NccPermissionsManager.showMultiplePermissionsDialogIfNeeded(this);

        realTimeSensorListener = (sensorType, data) -> {
            log(data.toString());
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        hasRequestedWhitelistingDuringForegroundSession = false;
    }

    private View.OnClickListener startListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button3:
                    Intent intent = new Intent(AdtStateMachine.ACTION_TRIGGER_UPDATE);
                    intent.putExtra(AdtStateMachine.TRIGGER_TYPE, AdtTriggerType.START_SERVER);
                    LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                    break;
                case R.id.button5:
                    nccSdk.startTracking();
                    break;
                case R.id.button7:
                    try {
                        nccSdk.addTrackingListener(realTimeSensorListener);
                    } catch (NccException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    private View.OnClickListener stopListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button4:
                    Intent intent = new Intent(AdtStateMachine.ACTION_TRIGGER_UPDATE);
                    intent.putExtra(AdtStateMachine.TRIGGER_TYPE, AdtTriggerType.STOP_SERVER);
                    LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
                    break;
                case R.id.button6:
                    nccSdk.stopTracking();
                    break;
                case R.id.button8:
                    try {
                        nccSdk.removeTrackingListener(realTimeSensorListener);
                    } catch (NccException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    private void log(String s) {
        Log.e(TAG, s);
    }

    public String getAppVersion(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
