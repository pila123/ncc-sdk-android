package com.agero.nccsdk.internal.data.network.aws;

import com.agero.nccsdk.internal.common.util.StringUtils;
import com.agero.nccsdk.internal.data.network.aws.iot.IotClient;
import com.agero.nccsdk.internal.data.preferences.NccSharedPrefs;
import com.agero.nccsdk.internal.data.preferences.SharedPrefs;
import com.agero.nccsdk.internal.log.Timber;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by james hermida on 9/19/17.
 */

@Singleton
public class RemoteMessageManager implements IotClient.OnConnectedCallback, AWSIotMqttNewMessageCallback {

    private final IotClient iotClient;
    private String lastUserIdSubscribedTo = "";
    private final SharedPrefs sharedPrefs;

    @Inject
    RemoteMessageManager(IotClient iotClient, NccSharedPrefs sharedPrefs) {
        this.iotClient = iotClient;
//        iotClient.addOnConnectedCallback(this);
        this.sharedPrefs = sharedPrefs;
    }

    @Override
    public void onConnected() {
        // Subscribe to user id topic
        // FIXME On first initialization of SDK, sharedPrefs.getUserId() may be empty since this class is initialized before the user id may be set
        // FIXME Subsequent calls to onConnected() will subscribe to the user id topic
        String userId = sharedPrefs.getUserId();
        if (!lastUserIdSubscribedTo.equalsIgnoreCase(userId)) {
            if (!StringUtils.isNullOrEmpty(userId)) {
                iotClient.subscribeToTopic(userId, this);
                lastUserIdSubscribedTo = userId;
            } else {
                Timber.e("No user id set to subscribe to topic"); // TODO listen to user id changes
            }
        }
    }

    @Override
    public void onMessageArrived(String topic, byte[] data) {
        try {
            String msg = new String(data, "UTF-8");
            Timber.d("New message received: %s", msg);
        } catch (UnsupportedEncodingException e) {
            Timber.e(e, "Exception handling message: %s", e.getMessage());
        }
    }
}
