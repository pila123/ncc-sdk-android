package com.agero.nccsdk.domain.sensor;

import android.content.Context;
import android.hardware.SensorEvent;

import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.config.NccConfig;
import com.agero.nccsdk.domain.data.NccAbstractSensorData;
import com.agero.nccsdk.domain.data.NccAccelerometerData;

/**
 * Created by james hermida on 8/21/17.
 */

public class NccAccelerometer extends NccAbstractNativeSensor {

    /**
     * Creates an instance of NccAccelerometer
     *
     * @param context The Context allowing access to application-specific resources and classes, as well as
     * up-calls for application-level operations such as launching activities,
     * broadcasting and receiving intents, etc.
     * @throws NccException Exception if there is an error constructing the instance
     */
    public NccAccelerometer(Context context, NccConfig config) throws NccException {
        super(context, NccAccelerometer.class.getSimpleName(), NccSensorType.ACCELEROMETER, config);
    }

    /**
     * See {@link NccAbstractNativeSensor#buildData(SensorEvent, long)}
     */
    @Override
    protected NccAbstractSensorData buildData(SensorEvent sensorEvent, long sensorEventTime) {
        return new NccAccelerometerData(sensorEventTime, sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]);
    }
}
