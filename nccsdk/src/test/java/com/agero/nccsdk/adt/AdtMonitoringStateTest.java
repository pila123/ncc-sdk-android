package com.agero.nccsdk.adt;

import android.Manifest;
import android.content.Context;

import com.agero.nccsdk.NccPermissionsManager;
import com.agero.nccsdk.adt.detection.domain.geofence.GeofenceManager;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.internal.common.util.DeviceUtils;
import com.agero.nccsdk.internal.data.DataManager;
import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by james hermida on 11/16/17.
 */

@RunWith(PowerMockRunner.class)
public class AdtMonitoringStateTest {

    private AdtMonitoringState adtMonitoringState;

    @Mock
    private Context mockContext;

    @Mock
    private AdtStateMachine mockAdtStateMachine;

    @Mock
    private GeofenceManager mockGeofenceManager;

    @Mock
    private DataManager mockDataManager;

    @Mock
    private SensorManager mockSensorManager;

    @Before
    public void setUp() throws Exception {
        adtMonitoringState = new AdtMonitoringState(mockContext);
    }

    @Test
    public void startDriving() {
        adtMonitoringState.startDriving(mockAdtStateMachine);
        verify(mockAdtStateMachine).setState(any(AdtDrivingState.class));
    }

    @Test
    public void stopDriving() {
        adtMonitoringState.stopDriving(mockAdtStateMachine);
        verifyNoMoreInteractions(mockAdtStateMachine);
    }

    @Test
    public void startMonitoring() {
        adtMonitoringState.startMonitoring(mockAdtStateMachine);
        verifyNoMoreInteractions(mockAdtStateMachine);
    }

    @Test
    public void stopMonitoring() {
        adtMonitoringState.stopMonitoring(mockAdtStateMachine);
        verify(mockAdtStateMachine).setState(any(AdtInactiveState.class));
    }

    @Test
    @PrepareForTest({NccPermissionsManager.class, DeviceUtils.class})
    public void createGeofenceIfNeeded() throws Exception {
        mockPrivateField(adtMonitoringState, "geofenceManager", mockGeofenceManager);

        PowerMockito.mockStatic(NccPermissionsManager.class);
        PowerMockito.when(NccPermissionsManager.class, "isPermissionAllowed", Manifest.permission.ACCESS_FINE_LOCATION).thenReturn(true);

        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.when(DeviceUtils.class, "isLocationSettingOnWithHighestAccuracy", mockContext).thenReturn(true);

        invokePrivateMethod("createGeofenceIfNeeded");

        verify(mockGeofenceManager).createGeofenceAtCurrentLocation();
    }

    @Test
    @PrepareForTest({NccPermissionsManager.class, DeviceUtils.class})
    public void createGeofenceIfNeeded_location_permission_not_allowed() throws Exception {
        mockPrivateField(adtMonitoringState, "geofenceManager", mockGeofenceManager);

        PowerMockito.mockStatic(NccPermissionsManager.class);
        PowerMockito.when(NccPermissionsManager.class, "isPermissionAllowed", Manifest.permission.ACCESS_FINE_LOCATION).thenReturn(false);

        invokePrivateMethod("createGeofenceIfNeeded");

        verify(mockGeofenceManager, times(0)).createGeofenceAtCurrentLocation();
    }

    @Test
    @PrepareForTest({NccPermissionsManager.class, DeviceUtils.class})
    public void createGeofenceIfNeeded_location_setting_not_high_accuracy() throws Exception {
        mockPrivateField(adtMonitoringState, "geofenceManager", mockGeofenceManager);

        PowerMockito.mockStatic(NccPermissionsManager.class);
        PowerMockito.when(NccPermissionsManager.class, "isPermissionAllowed", Manifest.permission.ACCESS_FINE_LOCATION).thenReturn(true);

        PowerMockito.mockStatic(DeviceUtils.class);
        PowerMockito.when(DeviceUtils.class, "isLocationSettingOnWithHighestAccuracy", mockContext).thenReturn(false);

        invokePrivateMethod("createGeofenceIfNeeded");

        verify(mockGeofenceManager, times(0)).createGeofenceAtCurrentLocation();
    }

    @Test
    @PrepareForTest({NccPermissionsManager.class})
    public void deleteGeofence() throws Exception {
        mockPrivateField(adtMonitoringState, "geofenceManager", mockGeofenceManager);
        PowerMockito.when(mockGeofenceManager.geofenceExists()).thenReturn(true);
        PowerMockito.when(mockGeofenceManager.getGeofenceLatLng()).thenReturn(new LatLng(0, 0));

        invokePrivateMethod("deleteGeofence");

        verify(mockGeofenceManager).deleteGeofence();
    }

    private void mockPrivateField(Object targetObject, String fieldName, Object fieldValue) throws Exception {
        Field field = AdtMonitoringState.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, fieldValue);
    }

    private void invokePrivateMethod(String methodName) throws Exception {
        Method method = AdtMonitoringState.class.getDeclaredMethod(methodName);
        method.setAccessible(true);
        method.invoke(adtMonitoringState);
    }
}
