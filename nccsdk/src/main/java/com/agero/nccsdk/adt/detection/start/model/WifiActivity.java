package com.agero.nccsdk.adt.detection.start.model;

/**
 * Created by james hermida on 12/4/17.
 */

public class WifiActivity {
    private WifiInfo connection;
    private WifiInfo disconnection;

    public WifiActivity() {
    }

    public WifiActivity(WifiInfo connection, WifiInfo disconnection) {
        this.connection = connection;
        this.disconnection = disconnection;
    }

    public WifiInfo getConnection() {
        return connection;
    }

    public void setConnection(WifiInfo connection) {
        this.connection = connection;
    }

    public WifiInfo getDisconnection() {
        return disconnection;
    }

    public void setDisconnection(WifiInfo disconnection) {
        this.disconnection = disconnection;
    }
}
