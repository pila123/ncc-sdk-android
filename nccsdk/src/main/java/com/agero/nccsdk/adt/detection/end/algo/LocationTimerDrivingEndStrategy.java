package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;
import android.os.Handler;

import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 11/14/17.
 */

public class LocationTimerDrivingEndStrategy extends DrivingEndStrategy<NccLocationData> implements Runnable {

    private final float THRESHOLD_LOCATION_SPEED;
    private final long TIMEOUT;

    private Handler timerHandler;
    
    public LocationTimerDrivingEndStrategy(Context context, long timeout, float speedThreshold) {
        super(context);
        TIMEOUT = timeout;
        THRESHOLD_LOCATION_SPEED = speedThreshold;
        scheduleStopCollectionBroadcast();
    }

    @Override
    public void evaluate(NccLocationData data) {
        if (data == null) {
            Timber.e("Unable to evaluate location. data is null");
        } else {
            // Whenever the speed is above the threshold,
            // reschedule the stop collection broadcast
            if (data.getSpeed() >= THRESHOLD_LOCATION_SPEED) {
                scheduleStopCollectionBroadcast();
            }
        }
    }

    @Override
    public void releaseResources() {
        if (timerHandler != null) {
            timerHandler.removeCallbacks(this);
        }
    }

    private void scheduleStopCollectionBroadcast() {
        if (timerHandler == null) {
            timerHandler = new Handler();
        } else {
            // Remove existing callback
            timerHandler.removeCallbacks(this);
        }

        // Post new collection end event
        timerHandler.postDelayed(this, TIMEOUT);
    }

    @Override
    public void run() {
        // When this runnable is executed, it means there
        // have been no locations that have broken the speed threshold
        // for the provided timeout duration
        sendCollectionEndBroadcast(AdtTriggerType.STOP_LOCATION_TIMER);
    }
}
