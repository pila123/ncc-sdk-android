package com.agero.nccsdk.internal.common.util;

import android.content.Context;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;

import com.agero.nccsdk.NccSdk;

import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 8/10/17.
 */

public class LocationUtils {
    
    public static boolean isDeviceUsingRequiredLocationSettings() {
        // Android 4.4 KitKat introduced Location modes for battery conservation
        // Required mode is High Accuracy in which GPS, Wi-Fi, and cellular networks
        // are used to determine location
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                int locationSetting = Settings.Secure.getInt(NccSdk.getApplicationContext().getContentResolver(), Settings.Secure.LOCATION_MODE);
                switch (locationSetting) {
                    case Settings.Secure.LOCATION_MODE_OFF:
                        Timber.v("Location setting: Off");
                        break;
                    case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
                        Timber.v("Location setting: GPS only");
                        break;
                    case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                        Timber.v("Location setting: Battery saving");
                        break;
                    case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
                        Timber.v("Location setting: High accuracy");
                        return true;
                }
            } catch (Settings.SettingNotFoundException snfe) {
                Timber.w(snfe, "Location mode setting not available on device");
            }

            // Otherwise location services must be on, and GPS and network location providers must be on
        } else {
            final LocationManager manager = (LocationManager) NccSdk.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            Timber.v("GPS_PROVIDER: %s NETWORK_PROVIDER: %s", manager.isProviderEnabled(LocationManager.GPS_PROVIDER), manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                return true;
            }
        }

        return false;
    }
    
}
