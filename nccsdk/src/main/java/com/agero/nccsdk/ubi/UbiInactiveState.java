package com.agero.nccsdk.ubi;

import android.content.Context;

import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.log.Timber;

/**
 * Created by james hermida on 9/6/17.
 */

public class UbiInactiveState extends UbiAbstractState {

    public UbiInactiveState(Context context) {
        super(context);
    }

    @Override
    public void onEnter() {
        Timber.d("Entered UbiInactiveState");
    }

    @Override
    public void onExit() {
        Timber.d("Exited UbiInactiveState");
    }

    @Override
    public void startCollecting(StateMachine<UbiState> machine) {
        machine.setState(new UbiCollectionState(applicationContext));
    }

    @Override
    public void stopCollecting(StateMachine<UbiState> machine) {
        // DO NOTHING
    }
}
