package com.agero.nccsdk.lbt;

import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.domain.SensorManager;
import com.agero.nccsdk.domain.config.NccLocationConfig;
import com.agero.nccsdk.internal.common.statemachine.StateMachine;
import com.agero.nccsdk.internal.data.DataManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

/**
 * Created by james hermida on 11/16/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class LbtCollectionStateTest {

    private LbtCollectionState lbtCollectionState;

    private NccSensorType[] defaultLbtSensors = {
            NccSensorType.LOCATION
    };

    @Mock
    private StateMachine<LbtState> mockLbtStateMachine;

    @Mock
    private SensorManager mockSensorManager;

    @Mock
    private DataManager mockDataManager;

    @Before
    public void setUp() throws Exception {
        List<NccSensorListener> listeners = new CopyOnWriteArrayList<>();
        lbtCollectionState = new LbtCollectionState(listeners);
        lbtCollectionState.sensorManager = mockSensorManager;
        lbtCollectionState.dataManager = mockDataManager;
    }

    @Test
    @PrepareForTest({LbtCollectionState.class, DataManager.class})
    @SuppressWarnings("unchecked")
    public void addTrackingListener() throws Exception {
        NccSensorListener sensorListener = (sensorType, data) -> {};
        mockPrivateField(lbtCollectionState, "defaultLbtSensors", defaultLbtSensors);
        NccLocationConfig testConfig = new NccLocationConfig();
        PowerMockito.when(mockDataManager.getConfig(defaultLbtSensors[0])).thenReturn(testConfig);

        lbtCollectionState.addTrackingListener(mockLbtStateMachine, sensorListener);
        List<NccSensorListener> listeners = (List<NccSensorListener>) getListeners(lbtCollectionState);
        assertTrue(listeners.size() == 1);
        assertTrue(listeners.contains(sensorListener));
        verify(mockSensorManager).subscribeSensorListener(defaultLbtSensors[0], sensorListener, testConfig);

        // Verify no interactions when the same listener is trying to added
        lbtCollectionState.addTrackingListener(mockLbtStateMachine, sensorListener);
        verifyNoMoreInteractions(mockSensorManager);
    }

    @Test
    @PrepareForTest({LbtCollectionState.class})
    @SuppressWarnings("unchecked")
    public void removeTrackingListener() throws Exception {
        NccSensorListener sensorListener = (sensorType, data) -> {};
        mockPrivateField(lbtCollectionState, "defaultLbtSensors", defaultLbtSensors);

        lbtCollectionState.addTrackingListener(mockLbtStateMachine, sensorListener);
        lbtCollectionState.removeTrackingListener(mockLbtStateMachine, sensorListener);
        List<NccSensorListener> listeners = (List<NccSensorListener>) getListeners(lbtCollectionState);
        assertFalse(listeners.contains(sensorListener));
        assertTrue(listeners.size() == 0);
        verify(mockSensorManager).unsubscribeSensorListener(defaultLbtSensors[0], sensorListener);
    }

    @Test
    public void removeTrackingListener_listener_never_added() throws Exception {
        NccSensorListener sensorListener = (sensorType, data) -> {};

        lbtCollectionState.removeTrackingListener(mockLbtStateMachine, sensorListener);
        verifyNoMoreInteractions(mockSensorManager);
    }

    @Test
    public void onDrivingStart() throws Exception {
        lbtCollectionState.onDrivingStart(mockLbtStateMachine);
        verifyZeroInteractions(mockLbtStateMachine);
    }

    @Test
    public void onDrivingStop() throws Exception {
        lbtCollectionState.onDrivingStop(mockLbtStateMachine);
        verify(mockLbtStateMachine).setState(any(LbtInactiveState.class));
    }

    private void mockPrivateField(Object targetObject, String fieldName, Object fieldValue) throws Exception {
        Field field = LbtCollectionState.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(targetObject, fieldValue);
    }

    private Object getListeners(LbtCollectionState collectionState) {
        try {
            Field f = getField(LbtCollectionState.class, "trackingListeners");
            f.setAccessible(true);
            f.get(collectionState);

            return f.get(collectionState);
        } catch (NoSuchFieldException nsfe) {
            nsfe.printStackTrace();
        } catch (IllegalAccessException iae) {
            iae.printStackTrace();
        }

        return null;
    }

    private Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }
}
