package com.agero.nccsdk.adt.detection.end.algo;

import android.content.Context;

import com.agero.nccsdk.domain.data.NccMotionData;
import com.agero.nccsdk.adt.AdtTriggerType;
import com.agero.nccsdk.internal.log.Timber;
import com.google.android.gms.location.DetectedActivity;

/**
 * Created by james hermida on 11/14/17.
 */

public class MotionDrivingEndStrategy extends DrivingEndStrategy<NccMotionData> {

    private final int THRESHOLD_MOTION_CONFIDENCE;
    
    public MotionDrivingEndStrategy(Context context, int threshold) {
        super(context);
        THRESHOLD_MOTION_CONFIDENCE = threshold;
    }

    @Override
    public void evaluate(NccMotionData data) {
        if (data == null) {
            Timber.e("Unable to evaluate motion for end strategy. data is null");
        } else if (data.getActivityType() == DetectedActivity.WALKING
                && data.getConfidence() >= THRESHOLD_MOTION_CONFIDENCE) {
            sendCollectionEndBroadcast(AdtTriggerType.STOP_MOTION);
        }
    }

    @Override
    public void releaseResources() {

    }
}
